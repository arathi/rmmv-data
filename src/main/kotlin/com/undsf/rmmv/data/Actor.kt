package com.undsf.rmmv.data

/**
 * Created by Arathi on 2017/12/12.
 */
data class Actor (var id: Int) {
    var battlerName: String = ""
    var characterIndex: Int = 0
    var characterName: String = ""
    var classId: Int = 0
    var equips: List<Int> = listOf()
    var faceIndex: Int = 0
    var faceName: String = ""
    var traits: List<Trait> = listOf()
    var initialLevel: Int = 1
    var maxLevel: Int = 99
    var name: String = ""
    var nickname: String = ""
    var note: String = ""
    var profile: String = ""
}
