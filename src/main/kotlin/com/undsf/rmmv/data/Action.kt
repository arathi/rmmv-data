package com.undsf.rmmv.data

data class Action(var conditionParam1: Int,
                  var conditionParam2: Int,
                  var conditionType: Int,
                  var rating: Int,
                  var skillId: Int) {

}
