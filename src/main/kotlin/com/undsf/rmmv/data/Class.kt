package com.undsf.rmmv.data

/**
 * Created by Arathi on 2017/12/12.
 */
data class Class(var id: Int) {
    // 基本值, 补正值, 增加度A, 增加度B
    var expParams: List<Int> = listOf()
    var traits: List<Trait> = listOf()
    var learnings: List<Skill> = listOf()
    var name: String = ""
    var note: String = ""
    var params: List<List<Int>> = listOf()
}