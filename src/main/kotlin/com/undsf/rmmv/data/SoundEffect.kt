package com.undsf.rmmv.data

data class SoundEffect(var name: String,
                       var pan: Int,
                       var pitch: Int,
                       var volume: Int) {
    constructor() : this("", 0, 0, 0)
}