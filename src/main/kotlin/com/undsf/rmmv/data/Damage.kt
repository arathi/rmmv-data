package com.undsf.rmmv.data

/**
 * Created by Arathi on 2017/12/12.
 */
class Damage(
    var critical: Boolean,
    var elementId: Int,
    var formula: String,
    var type: Int,
    var variance: Int) {
    constructor() : this(
            false,
            -1,
            "a.atk * 4 - b.def * 2",
            0,
            0
    )
}