package com.undsf.rmmv.data

data class State(var id: Int) {
    var autoRemovalTiming: Int = 0
    var chanceByDamage: Int = 0
    var iconIndex: Int = 0
    var maxTurns: Int = 0
    var message1: String = ""
    var message2: String = ""
    var message3: String = ""
    var message4: String = ""
    var minTurns: Int = 0
    var motion: Int = 0
    var name: String = ""
    var note: String = ""
    var overlay: Int = 0
    var priority: Int = 0
    var releaseByDamage: Boolean = false
    var removeAtBattleEnd: Boolean = false
    var removeByDamage: Boolean = false
    var removeByRestriction: Boolean = false
    var removeByWalking: Boolean = false
    var restriction: Int = 0
    var stepsToRemove: Int = 0
    var traits: List<Trait> = listOf()
}