package com.undsf.rmmv.data

/**
 * Created by Arathi on 2017/12/12.
 */
data class Armor(var id: Int) {
    var atypeId: Int = 0
    var description: String = ""
    var etypeId: Int = 0
    var traits: List<Trait> = listOf()
    var iconIndex: Int = 0
    var name: String = ""
    var note: String = ""
    var params: List<Int> = listOf()
    var price: Int = 0
}