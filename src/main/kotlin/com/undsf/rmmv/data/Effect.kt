package com.undsf.rmmv.data

/**
 * Created by Arathi on 2017/12/12.
 */
data class Effect(var code: Int,
                  var dataId: Int,
                  var value1: Int,
                  var value2: Int) {
}