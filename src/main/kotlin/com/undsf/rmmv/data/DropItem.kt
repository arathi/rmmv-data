package com.undsf.rmmv.data

data class DropItem(var dataId: Int,
                    var denominator: Int,
                    var kind: Int) {
}