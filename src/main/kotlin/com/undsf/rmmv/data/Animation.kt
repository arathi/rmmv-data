package com.undsf.rmmv.data

data class Animation(var id:Int) {
    var animation1Hue: Int = 0
    var animation1Name: String = ""
    var animation2Hue: Int = 0
    var animation2Name: String = ""
    var frames: List<List<Int>> = listOf()
    var name: String = ""
    var position: Int = 0
    var timings: List<Timing> = listOf()
}