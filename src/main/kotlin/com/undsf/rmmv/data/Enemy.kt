package com.undsf.rmmv.data

data class Enemy(var id: Int) {
    var actions: List<Action> = listOf()
    var battlerHue: Int = 0
    var battlerName: String = ""
    var dropItems: List<DropItem> = listOf()
    var exp: Int = 0
    var traits: List<Trait> = listOf()
    var gold: Int = 0
    var name: String = ""
    var note: String = ""
    // hp, mp, patk, pdef, matk, mdef, agi, luck
    var params: List<Int> = listOf()
}