package com.undsf.rmmv.data

class Timing {
    // ARGB或者RGBA
    var flashColor: List<Int> = listOf()
    var flashDuration: Int = 0
    var flashScope: Int = 0
    var frame: Int = 0
    var se: SoundEffect = SoundEffect()
}