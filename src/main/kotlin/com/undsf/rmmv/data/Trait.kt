package com.undsf.rmmv.data

/**
 * Created by Arathi on 2017/12/12.
 */
data class Trait(var code: Int,
                 var dataId: Int,
                 var value: Int) {
}