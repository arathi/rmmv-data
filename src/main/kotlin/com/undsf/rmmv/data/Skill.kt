package com.undsf.rmmv.data

/**
 * Created by Arathi on 2017/12/12.
 */
data class Skill(var id: Int) {
    var animationId: Int = 0
    var damage: Damage = Damage()
    var description: String = ""
    var effects: List<Effect> = listOf()
    var hitType: Int = 0
    var iconIndex: Int = 0
    var message1: String = ""
    var message2: String = ""
    var mpCost: Int = 0
    var name: String = ""
    var note: String = ""
    var occasion: Int = 0
    var repeats: Int = 0
    var requiredWtypeId1: Int = 0
    var requiredWtypeId2: Int = 0
    var scope: Int = 0
    var speed: Int = 0
    var stypeId: Int = 0
    var successRate: Int = 0
    var tpCost: Int = 0
    var tpGain: Int = 0
}