package com.undsf.rmmv.data

data class Weapon(var id: Int) {
    var animationId: Int = 0
    var description: String = ""
    var etypeId: Int = 0
    var traits: List<Trait> = listOf()
    var iconIndex: Int = 0
    var name: String = ""
    var note: String = ""
    var params: List<Int> = listOf()
    var price: Int = 0
    var wtypeId: Int = 0
}