package com.undsf.rmmv.data

data class Item(var id: Int) {
    var animationId: Int = 0
    var consumable: Boolean = false
    var damage: Damage = Damage()
    var description: String = ""
    var effects: List<Effect> = listOf()
    var hitType: Int = 0
    var iconIndex: Int = 0
    var itypeId: Int = 0
    var name: String = ""
    var note: String = ""
    var occasion: Int = 0
    var price: Int = 0
    var repeats: Int = 0
    var scope: Int = 0
    var speed: Int = 0
    var successRate: Int = 0
    var tpGain: Int = 0
}